package ictgradschool.industry.concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable {
    private long n;
    TaskState taskState;
    private List<Long> primeList;

    public PrimeFactorsTask(long n) {
//        PrimeFactorsTask p = new PrimeFactorsTask(n);
        taskState = TaskState.Initialized;
        this.n = n;
    }

    @Override
    public void run() {
        primeList = new ArrayList<>();
        for (long i = 2; i * i <= n; i++) {
            if(Thread.currentThread().isInterrupted()){
                System.out.print("break");
                break;
            }
            // If i is a factor of N, repeatedly divide it out
            while (n % i == 0) {
                primeList.add(i);
//						_factorValues.append(i + "\n");
                n = n / i;
            }
        }

        // if biggest factor occurs only once, n > 1
        if (n > 1) {
            primeList.add(n);
//					_factorValues.append(n + "\n");
        }
        taskState = TaskState.Completed;
    }
    public long n(){
        return n;
    }

    public List<Long> getPrimeList() throws IllegalStateException {

        return primeList;

    }
    public TaskState setState(TaskState TaskState) {
        return TaskState;
    }

}
