package ictgradschool.industry.concurrency.ex05;

public enum TaskState {
    Initialized, Completed, Aborted
}
