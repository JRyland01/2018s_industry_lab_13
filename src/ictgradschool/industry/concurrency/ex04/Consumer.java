package ictgradschool.industry.concurrency.ex04;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {
    private BlockingQueue<Transaction> queue;
    private BankAccount myBankAcoount = new BankAccount();

    public Consumer (BankAccount b, BlockingQueue<Transaction> queue){
        this.myBankAcoount = b;
        this.queue = queue;
    }
    @Override
    public void run(){
        try {
            while (true){
                Transaction current  = queue.take();
                if (current._type.equals(Transaction.TransactionType.Withdraw)){
                    myBankAcoount.withdraw(current._amountInCents);
                }else if (current._type.equals(Transaction.TransactionType.Deposit)){
                    myBankAcoount.deposit(current._amountInCents);
                }
            }
        }catch(InterruptedException e) {}

    }
}
