package ictgradschool.industry.concurrency.ex04;

import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrentBankingApp {

    public static void main(String[] args) {
        BankAccount accountMain =  new BankAccount();
        final BlockingQueue <Transaction> queue = new ArrayBlockingQueue<>(10);
        Thread producer = new Thread(new Runnable (){
            @Override
            public void run(){
                List<Transaction> transactionsOngoing= TransactionGenerator.readDataFile();
                for(Transaction t : transactionsOngoing) {
                    try {
                        queue.put(t);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        Thread consumer1 = new Thread(new Consumer(accountMain,queue));
        Thread consumer2 = new Thread(new Consumer(accountMain,queue));
        producer.start();

        consumer1.start();
        consumer2.start();
        try {
            producer.join();

           while(queue.poll() != null){
           }

            consumer1.interrupt();
            consumer2.interrupt();
            consumer1.join();
            consumer2.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Final balance: " + accountMain.getFormattedBalance());
    }
}
