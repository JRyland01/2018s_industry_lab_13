package ictgradschool.industry.concurrency.ex01;

import ictgradschool.Keyboard;

public class Ex1Runnable {
    public void start (){
        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                for(int i=0;i<1000000;i++){
                    System.out.println(i);
                }
            }
        };
        Thread thread = new Thread(myRunnable);
        thread.start();
        System.out.println("Press enter to terminate thread");
        Keyboard.readInput();
        thread.interrupt();
        try {
            thread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("termination complete.");
    }
    public static void main(String[] args) {
        new Ex1Runnable().start();
    }
}
