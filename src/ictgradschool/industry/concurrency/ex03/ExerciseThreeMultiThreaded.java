package ictgradschool.industry.concurrency.ex03;

import ictgradschool.Keyboard;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {
    private double multiPI;
    private double randoPI;
    private int i;
    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected void start() {

        System.out.println("Enter the number of samples to use for approximation:");
        System.out.print("> ");

        long numSamples = Long.parseLong(Keyboard.readInput());

        System.out.println("Estimating PI...");
        long startTime = System.currentTimeMillis();

        // Do the estimation
        try {
            multiPI = estimatePI(numSamples);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        long endTime = System.currentTimeMillis();
        long timeInMillis = endTime - startTime;

        double difference = Math.abs(multiPI - Math.PI);
        double differencePercent = 100.0 * difference / Math.PI;
        NumberFormat format = new DecimalFormat("#.####");

        System.out.println("Estimate of PI: " + multiPI);
        System.out.println("Estimate is within " + format.format(differencePercent) + "% of Math.PI");
        System.out.println("Estimation took " + timeInMillis + " milliseconds");

    }
    @Override
    protected double estimatePI(long numSamples) throws InterruptedException {
        List <Thread> myThread = new ArrayList<Thread>();
        Vector <Double> myPIList = new Vector<>();

        for(i =0 ; i <= 100; i++){
            Thread t = new Thread ( new Runnable () {
                @Override
                public void run () {
                    try {
                        randoPI = ExerciseThreeMultiThreaded.super.estimatePI(numSamples);
                        myPIList.add(randoPI);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });
            t.start();
            myThread.add(t);
        }
        for ( Thread threads : myThread) {
            threads.join();
        }
        double multi=0.0;
        System.out.println(myPIList);
        for(Double d : myPIList){
            multi += d;
        }
        return multi/100;
        // TODO Implement this.

    }

    /** Program entry point. */
    public static void main(String[] args) {
        new ExerciseThreeMultiThreaded().start();
    }
}
